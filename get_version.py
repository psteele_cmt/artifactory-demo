from datetime import date, datetime
import logging
from pathlib import Path
import pkg_resources
from pkg_resources.extern.packaging.version import Version
import re
from typing import List
import subprocess
import sys

import toml


logger = logging.getLogger(__name__)


_date_format: str = "%Y%m%d"


def date_version_string(x: date) -> str:
    """Convert a date to a string that can be used in package versions.

    This is the inverse of version_string_date.

    Examples
    --------

    >>> date_version_string(date(2020, 2, 29))
    '20200229'
    >>> date_version_string(date(2020, 6, 1))
    '20200601'
    >>> date_version_string(date(2020, 12, 31))
    '20201231'

    """
    return x.strftime(_date_format)


def version_string_date(x: str) -> date:
    """Compare a version string representing a date to a date.

    This is the inverse of date_version_string.

    Examples
    --------

    >>> version_string_date('20200229')
    datetime.date(2020, 2, 29)
    >>> version_string_date('20200601')
    datetime.date(2020, 6, 1)
    >>> version_string_date('20201231')
    datetime.date(2020, 12, 31)

    """
    return datetime.strptime(x, _date_format).date()


def next_version(major_version: int, minor_version: int, existing_versions: List[Version]) -> Version:
    """Find the next version, given major, minor, and existing versions.

    Examples
    --------

    >>> next_version(0, 0, [])
    <Version('0.0.0')>
    >>> next_version(0, 0, [Version('0.0.0')])
    <Version('0.0.1')>
    >>> next_version(0, 0, [Version('0.0.0'), Version('0.0.1')])
    <Version('0.0.2')>
    >>> next_version(0, 1, [Version('0.0.0'), Version('0.0.1'), Version('0.0.2')])
    <Version('0.1.0')>
    >>> next_version(1, 0, [Version('0.0.0'), Version('0.0.1'), Version('0.0.2'), Version('0.1.0')])
    <Version('1.0.0')>

    This works with date-based versions as well:

    >>> next_version(1, 20200605, [Version('1.20200605.0')])
    <Version('1.20200605.1')>

    """

    # Find the highest micro version among versions that match the
    # major and minor versions
    highest_micro = -1
    for version in existing_versions:
        try:
            major, minor, micro = map(int, str(version).split("."))
            if major == major_version and minor == minor_version:
                highest_micro = max(highest_micro, micro)
        except Exception:
            logger.exception("Failed to parse version %s", version)

    return Version(f"{major_version}.{minor_version}.{highest_micro + 1}")


def get_pyproject_major_version(pyproject_file: Path) -> int:
    with open(pyproject_file) as f:
        data = toml.load(f)

    return int(data["tool"]["poetry"]["version"])


def get_artifactory_versions(project: str) -> List[Version]:
    commands = [sys.executable, "-m", "pip", "--no-cache-dir", "install", f"{project}==", "--force"]

    result = subprocess.run(commands, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, check=False, text=True,)
    pip_output = result.stdout.replace("\n", "")

    no_match_regex = re.compile(".*\\(from versions:[ ]none\\)")

    if no_match_regex.match(pip_output) is not None:
        return []

    versions_regex = re.compile(".*\\(from versions:([^)]*)")
    version_string = versions_regex.match(pip_output).group(1)

    return [pkg_resources.parse_version(v) for v in version_string.split(",")]


if __name__ == "__main__":
    main_logger = logging.getLogger("")
    main_logger.addHandler(logging.StreamHandler(sys.stderr))
    main_logger.setLevel(logging.WARNING)

    print(
        next_version(
            get_pyproject_major_version("pyproject.toml"),
            date_version_string(date.today()),
            get_artifactory_versions("artifactory-demo"),
        )
    )
