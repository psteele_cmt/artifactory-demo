from datetime import date
import pkg_resources
from pkg_resources.extern.packaging.version import Version
import re
from typing import List

from hypothesis import example, given, strategies as st

from get_version import date_version_string, next_version, version_string_date


@st.composite
def arbitrary_version(draw) -> Version:
    major = draw(st.integers(0, 20))
    minor = draw(st.integers(0, 20))
    micro = draw(st.integers(0, 20))

    return pkg_resources.parse_version(f"{major}.{minor}.{micro}")


def test_date_version_string_manual_tests() -> None:
    """Verify that date_version_string performs as expected on
    hand-selected inputs.

    """

    assert "20200601" == date_version_string(date(2020, 6, 1))
    assert "20201231" == date_version_string(date(2020, 12, 31))


@given(st.dates(date(2000, 1, 1), date(2100, 1, 1)))
def test_date_version_roundtrip(x: date) -> None:
    """Verify that date_version_string and version_string_date are
    inverses of each other.

    """

    assert x == version_string_date(date_version_string(x))


def test_version_string_manual_tests() -> None:
    """Verify version_string performs as expected on hand-selected inputs."""

    # Verify that builds on the same date work as expected
    assert Version("0.20200605.0") == next_version(0, int(date_version_string(date(2020, 6, 5))), [])
    assert Version("0.20200605.1") == next_version(
        0, int(date_version_string(date(2020, 6, 5))), [Version("0.20200605.0")]
    )
    assert Version("0.20200605.9") == next_version(
        0, int(date_version_string(date(2020, 6, 5))), [Version("0.20200605.0"), Version("0.20200605.8")]
    )

    assert Version("0.20200606.0") == next_version(0, int(date_version_string(date(2020, 6, 6))), [])
    assert Version("0.20200606.0") == next_version(
        0, int(date_version_string(date(2020, 6, 6))), [Version("0.20200605.0")]
    )
    assert Version("0.20200606.0") == next_version(
        0, int(date_version_string(date(2020, 6, 6))), [Version("0.20200605.0"), Version("0.20200605.8")]
    )


@given(st.integers(0, 20), st.integers(0, 20), st.lists(arbitrary_version()))
def test_version_string(major_version: int, minor_version: int, existing_versions: List[Version]) -> None:
    """Verify that new versions satisfy required properties.

    We require that

      1. Versions look like N.N.N, with the specified major and minor versions.

      2. Build versions are unique

      3. For a given major version and build date, the new build date
         is strictly larger than any previous build version

      4. For a given major version and build date, the new build date
         is one larger than the previous largest build number

    """

    version = next_version(major_version, minor_version, existing_versions)

    assert isinstance(version, Version)

    version_string = str(version)

    # Verify property 1
    assert version_string.startswith(f"{major_version}.{minor_version}")
    assert re.match("\\d+[.]\\d+[.]\\d+", version_string) is not None

    # Verify property 2
    assert version not in existing_versions

    new_micro = int(version_string.split(".")[-1])
    closest_micro = -1
    for existing in existing_versions:
        ex_major, ex_minor, ex_micro = map(int, str(existing).split("."))

        if ex_major == major_version and ex_minor == minor_version:
            # Verify property 3
            assert ex_micro < new_micro

            closest_micro = max(closest_micro, ex_micro)

    # Verify property 4
    assert new_micro == closest_micro + 1
