#!/usr/bin/env bash

python3 -m venv poetry-venv
source poetry-venv/bin/activate
pip install poetry

poetry install --no-root
poetry version `poetry run python get_version.py`
poetry build
